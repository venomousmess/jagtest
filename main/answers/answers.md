#JAG Method software developer assessment
## Answers

### 1. SEO (5min)

1) URL Routing

2) Use title attribute for <a href> tags

3) Use RSS where possible

4) Use more weighted markup tags more frequently

### 2. Responsive (15m)

1) Change bootstrap.css, modify "section" element to have background-repeat: no-repeat; and background-size: auto;

2) Change bootstrap.css, modify ".col-md-3" class to have background-size: cover; float: none; /*width: 25%;*/

3) Modify <div> with id=quote, add .col-md-3 class to element.

4) Change bootstrap.css, modify ".col-lg-3 {/*width: 25%;*/}"


### 3. Validation (15m)
Add any special implemetation instructions here.

### 4. JavaScript (20m)
Add any special implemetation instructions here.

### 5. Ajax calls (30m)
Add any special implemetation instructions here.

### 6. Call a REST webservice (25m)
Add any special implemetation instructions here.

Make sure that the WebHost calls the ServiceHost via REST.

### 7. ADO.Net (40m)
Add any SQL schema changes here

### 8. Poll DB (15m)
Add any SQL schema changes here

Make changes ServiceHost

### 9. SignalR (40m)
Add any SQL schema changes here

### 10. Data Analysis (30m)

1) Total Profit
**Answer**

**SQL**
`Select....`

2) Total Profit (Earnings less VAT)
**Answer**

**SQL**
`Select....`

3) Profitable campaigns
**Answer**

**SQL**
`Select....`

4) Average conversion rate
**Answer**

**SQL**
`Select....`

5) Pick 2 clients based on Profit & Conversion rate & Why?
**Answer**

**SQL**
`Select....`
