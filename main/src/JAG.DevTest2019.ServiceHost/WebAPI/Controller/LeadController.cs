﻿using JAG.DevTest2019.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace JAG.DevTest2019.LeadService.Controllers
{
    public class LeadController : ApiController
    {
        [HttpPost]
        [ResponseType (typeof(LeadResponse))]
        public HttpResponseMessage Post(Lead request)
        {
            LeadResponse response = new LeadResponse()
            {
                LeadId = 10000000 + new Random().Next(),
                IsCapped = false,
                Messages = new[] { "Success from WebAPI" },
                IsSuccessful = true,
                IsDuplicate = false
            };

            //TODO: 7. Write the lead to the DB

            ICollection<ServiceHost.LeadParameter> parameters = new List<ServiceHost.LeadParameter>();
            if (request.LeadParameters != null)
            {
                foreach (var leadParam in request.LeadParameters)
                {
                    var leadParameter = new ServiceHost.LeadParameter()
                    {
                        LeadId = request.LeadId,
                        LeadParameterId = long.Parse(leadParam.Value)
                    };
                    parameters.Add(leadParameter);
                }
            }

            ServiceHost.Lead newLead = new ServiceHost.Lead()
            {
                LeadId = request.LeadId,
                FirstName = request.FirstName,
                LastName = request.Surname,
                Email = request.EmailAddress,
                ContactNumber = request.ContactNumber,
                LeadParameters = parameters
            };

            using (var dbContext = new DbContext("JAG2019"))
            {
                dbContext.Database.Connection.Open();
                var leads = dbContext.Set<ServiceHost.Lead>();
                leads.Add(newLead);

                dbContext.SaveChanges();
                dbContext.Database.Connection.Close();
            }

            Console.WriteLine($"Lead received {request.FirstName} {request.Surname}");

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
    }
}
