//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JAG.DevTest2019.ServiceHost
{
    using System;
    using System.Collections.Generic;
    
    public partial class LeadParameter
    {
        public long LeadParameterId { get; set; }
        public long LeadId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    
        public virtual Lead Lead { get; set; }
    }
}
