﻿using JAG.DevTest2019.ServiceHost.WebAPI;
using Microsoft.Owin.Hosting;
using System;
using static PollingLibrary.Polling;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using PollingLibrary;
using System.Data.Entity;

namespace JAG.DevTest2019.ServiceHost
{
    class Program
    {
        private static IDisposable _WebApiServiceHost;

        static void Main(string[] args)
        {
            Console.WriteLine($"Starting WebAPI");
            int apiPort = 8099;
            string apiHost = "http://localhost";
            string url = $"{apiHost}:{apiPort}";

            _WebApiServiceHost = WebApp.Start<WebApiStartup>(url);
            Console.WriteLine($"WebAPI hosted on {url}");

            try
            {
                var leadCount = Poll(() => GetLeadCount(), 3600);
                Console.WriteLine(leadCount);
            }
            catch (PollingTimeoutException ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine($"Press enter to exit");
            Console.ReadLine();
        }

        static int GetLeadCount()
        {
            DbSet<ServiceHost.Lead> leads = null;
            using (var dbContext = new DbContext("JAG2019"))
            {
                dbContext.Database.Connection.Open();
                leads = dbContext.Set<ServiceHost.Lead>();
            }

            if (leads == null)
            {
                return 0;
            }
            return leads.Count();
        }
    }
}
