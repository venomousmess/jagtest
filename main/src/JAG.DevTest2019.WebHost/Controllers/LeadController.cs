﻿using JAG.DevTest2019.Host.Models;
using JAG.DevTest2019.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace JAG.DevTest2019.Host.Controllers
{
    public class LeadController : Controller
    {

        public ActionResult Index()
        {
            return View(new LeadViewModel());
        }

        [HttpPost]
        public ActionResult SubmitQuoteDetails(LeadViewModel model)
        {
            if (ModelState.IsValid)
            {
                //TODO: Submit details for processing
                return RedirectToAction("SubmitLead");
            }
            return View("Index");
        }

        [HttpPost]
        public async Task<ActionResult> SubmitLead(LeadViewModel model)
        {
            //TODO: 6. Call the WebAPI service here & pass results to UI

            Lead leadForSubmittion = new Lead()
            {
                FirstName = model.FirstName,
                Surname = model.Surname,
                EmailAddress = model.EmailAddress,
                ContactNumber = model.ContactNumber
            };

            /*--- Post, asynchronusly to /api/lead REST endpoint  ---*/
            using (var apiClient = new HttpClient())
            {
                try
                {
                   
                    HttpResponseMessage response = await apiClient.PostAsJsonAsync("http://localhost:8099/api/lead", leadForSubmittion);
                    Console.WriteLine(response);

                    if(response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        LeadViewModel result = new LeadViewModel()
                        {
                            Results = new LeadResultViewModel()
                            {
                                LeadId = new Random().Next(),
                                IsSuccessful = true,
                                Message = "Thank you for submitting your details"
                            }
                        };

                        return View("Index", result);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            return View("Index");
        }
    }
}